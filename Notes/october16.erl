%%% Erlang notes from October 16 lecture %%%

-module('october16').
-compile(export_all).


-record(person, {name, age}).

%% use rr(module_name) to load records into REPL
aPerson() ->
		#person{name="Tom",age=23}.

%% Recursive data structures
% Trees
% {empty}
% {node, Data, LeftTree, Righttree}
mkLeaf(N) ->
		{node, N, {empty}, {empty}}.

aTree() ->
		{node, 7, mkLeaf(2), {node, 9, mkLeaf(8), {empty}}}.

sizeT({empty}) ->
		0;
sizeT({node, _D, LT, RT}) ->
		1 + sizeT(LT) + sizeT(RT).

sumT({empty}) ->
		0;
sumT({node, D, LT, RT}) ->
		D + sumT(LT) + sumT(RT).

mapT(_F, {empty}) ->
		{empty};
mapT(F, {node, D, LT, RT}) ->
		{node, F(D), mapT(F, LT), mapT(F, RT)}.

foldT(_F, A, {empty}) ->
		A;
foldT(F, A, {node, D, LT, RT}) ->
		F(D, foldT(F, A, LT), foldT(F, A, RT)).

% Pre-order traversal
preT() ->
		foldT(fun (X, AL, AR) ->
									[X| AL++AR] end, [], aTree()).

% In-order traversal
inT() ->
		foldT(fun (X, AL, AR) ->
									[AL++X++AR] end, [], aTree()).
% Post-order traversal
postT() ->
		foldT(fun (X, AL, AR) ->
									[AL++AR|X] end, [], aTree()).

% Mirror image of a tree
mirror(T) ->
		foldT(fun(D, ML, MR) -> {node, D, MR, ML} end, {empty}, T).

% Map on a tree using fold
mapT2(F, T) ->
		foldT(fun(X, ML, MR) -> {node, F(X), ML, MR} end, {empty}, T).
