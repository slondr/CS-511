-module('erlang-notes').
-compile(export_all).

fact(0) ->
    1;
fact(N) ->
    N*fact(N-1).

len([]) ->
    0;
len([_|T]) ->
    1+len(T).

sum([]) ->
    0;
sum([H|T]) ->
    H + sum(T).

map(_F, []) ->
    [];
map(F, [H|T]) ->
    [F(H) | map(F, T)].

foldl(_F, A, []) ->
    A;
foldl(F, A, [H|T]) ->
    F(H, foldl(F, A, T)).


foldr(_F, A, []) ->
    A;
foldr(F, A, [H|T]) ->
    foldr(F, F(A, H), T).

%% checks if X is a member of L
mem(_X, []) ->
    false;
mem(X, [H|T]) ->
    if X == H ->
	    true;
       true ->
	    mem(X, T)
    end.

%% sublist(L1, L2) checks if L2 is a sublist of L1
sublist(_L1, []) ->
    false;
sublist(L1, [H|T]) ->
     if erlang-notes:mem(H, L1) ->
	     sublist(L1, T);
	true ->
	     false
     end.

    
