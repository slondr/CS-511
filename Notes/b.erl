-module(b).
-compile(export_all).

barrier(0, N, L) -> % Notify all threads they can proceed
		case L of
				[H|T] ->
						H!{self(), ok},
						barrier(0, N, T);
				[] ->
						barrier(N, N, [])
		end;
barrier(M, N, L) when M > 0 -> % Wait for another thread then register it in L
		receive
				{Ref, reached} ->
						barrier(M - 1, N, L ++ [Ref])
		end.

pass_barrier(B) ->
		B!{self(), reached},
		receive
				{B, ok} ->
						ok
		end.

client(B, Letter, Number) ->
		io:format("~p ~s~n", [self(), Letter]),
		pass_barrier(B),
		io:format("~p ~w~n", [self(), Number]).

start() ->		
		B = spawn(?MODULE, barrier, [3, 3, []]),
		spawn(?MODULE, client, [B, "a", 1]),
		spawn(?MODULE, client, [B, "b", 2]),
		spawn(?MODULE, client, [B, "c", 3]).

%%% Producers/Consumers

buffer(Size, Consumers, Producers, Capacity) ->
		receive
				{From, startProduce} when Size + Producers < Capacity ->
						From!{self(), ok},
						buffer(Size, Consumers, Producers + 1, Capacity);
				{From, stopProduce} ->
						buffer(Size + 1, Consumers, Producers - 1, Capacity);				
				{From, startConsume} when Size > Consumers ->
						From!{self(), ok},
						buffer(Size, Consumers + 1, Producers, Capacity);
				{From, stopConsume} ->
						buffer(Size - 1, Consumers - 1, Producers, Capacity)
		end.

producer(B) ->
		B!{self(), startProduce},
		receive
				{B, ok} ->
						io:format("~p: produce~n", [self()]),
						B!{self(), stopProduce}
		end.

consumer(B) ->
		B!{self(), startConsume},
		receive
				{B, ok} ->
						io:format("~p: consume~n", [self()]),
						B!{self(), stopConsume}
		end.

start(NP, NC, Capacity) ->
		B = spawn(?MODULE, buffer, [0, 0, 0, Capacity]),
		[spawn(?MODULE, producer, [B]) || _ <- lists:seq(1, NP)],
		[spawn(?MODULE, consumer, [B]) || _ <- lists:seq(1, NC)].
																			
