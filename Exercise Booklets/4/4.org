#+TITLE: Exercise Booklet 4
#+SUBTITLE: Semaphores
#+AUTHOR: Eric S. Londres
#+OPTIONS: toc:nil
#+LATEX_HEADER: \usepackage[margin=0.4in]{geometry}

- 1 ::
  + a ::
	 #+BEGIN_SRC java
	 Semaphore mutex = new Semaphore(0);
	 thread: {                  thread: {
	   print('A');                print('E');
	   mutex.release();           mutex.acquire();
	   print('B');                print('F');
	   print('C');                print('G');
	 }                          }
	 #+END_SRC
  + b ::
	 #+BEGIN_SRC java
	 Semaphore mutex = new Semaphore(0);
	 thread: {                  thread: {
	   print('A');                print('E');
	   print('B');                print('F');
	   mutex.acquire();           mutex.release();
	   print('C');                print('G');
	 }                          }
	 #+END_SRC
- 2 ::
       #+BEGIN_SRC java
       Semaphore mutex1 = new Semaphore(0);
       Semaphore mutex2 = new Semaphore(0);
       
       thread: {
         mutex1.acquire();
	 print('A');
	 print('C');
	 mutex2.release();
       }
       
       thread: {
         print('R');
         mutex1.release();
         mutex2.acquire();
         print('E');
         print('S');
       }
       #+END_SRC
- 3 ::
       #+BEGIN_SRC java
       Semaphore a = new Semaphore(0);
       Semaphore b = new Semaphore(0);
       Semaphore c = new Semaphore(0);

       thread: {       thread: {       thread: {
         print("R");     a.acquire();    b.acquire();
	 a.release();    print("I");     print("O");
	 c.acquire();    b.release();    c.release();
	 print("OK");    c.acquire();    c.release();
	                 print("OK");    print("OK");
       }               }               }
       #+END_SRC
- 4 :: The possible final values for =x= are 0, 1, and 3.
- 9 :: 
