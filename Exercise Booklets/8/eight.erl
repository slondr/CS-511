-module(eight).
-compile(export_all).

start() ->
		spawn(fun server/0).

servlet(Cl, N) ->
		receive
				{Cl, Ref, guess, N} ->
						Cl!{self(), Ref, gotIt};
				{Cl, Ref, guess, _Number} ->
						Cl!{self(), Ref, tryAgain},
						servlet(Cl, N)
		end.


server() ->
		receive
				{From, Ref, start} ->
						S = spawn(?MODULE, servlet, [From, rand:uniform(10)]),
						From!{self(), Ref, S},
						server();
				{stop} -> stop;
				_ -> server()
		end.

client(S) ->
		R = make_ref(),
		S!{self(), R, start},
		receive
				{S, R, Servlet} ->
						client_loop(Servlet, 0)
		end.

client_loop(Servlet, C) ->
		R = make_ref(),
		Servlet!{self(), R, guess, rand:uniform(10)},
		receive
				{Servlet, R, gotIt} ->
						io:format("Client ~p guessed in ~w attempts~n", [self(), C]);
				{Servlet, R, tryAgain} ->
						client_loop(Servlet, C + 1)
		end.
						 
