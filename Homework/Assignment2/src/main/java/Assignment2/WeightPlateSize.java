package Assignment2;

/**
 * This enum defines the sizes of the weight places, and defines a random
 * generator to return a random weight plate size
 */
public enum WeightPlateSize {
	SMALL_3KG, MEDIUM_5KG, LARGE_10KG;
	
	public static WeightPlateSize generateRandomWeightPlateSize() {
		java.util.Random random = new java.util.Random();
		final int rnd = random.nextInt(3);
		return values()[rnd];
	}
}
