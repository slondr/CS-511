package Assignment2;

import java.util.List;
import java.util.Random;
import java.util.Stack;

/**
 * The Client class represents clients of the gym.
 * Each client has an id and a routine. Both of these will be created randomly
 * at runtime as the simulation executes.
 */
public class Client implements Runnable {
	private int id;
	private List<Exercise> routine;
	
	public Client(int id) {
		this.id = id;
		routine = new Stack<Exercise>();
	}
	
	/**
	 * Creates a new Client with a specified ID. Generates 15 to 20 random
	 * exercises, and adds those exercises to the new client's routine.
	 *
	 * @param id The client's ID to assign to the returned client.
	 * @return The generated client.
	 */
	public static Client generateRandom(int id) {
		Client ret = new Client(id);
		Random random = new Random();
		final int bound = 15 + random.nextInt(5);
		for (char i = 0; i < bound; ++i)
			ret.addExercise(Exercise.generateRandom());
		return ret;
	}
	
	public void addExercise(Exercise e) {
		try {
			routine.add(e);
		} catch (Exception exception) {
			System.err.println("Failed to add exercise to routine list.");
			exception.printStackTrace();
		}
	}
	
	public void run() {
		System.out.println(String.format("Client #%d has arrived at the gym.", this.id));
		for (Exercise exercise : routine) {
			System.out.println(String.format("Client #%d is about to start (%d of %d).", this.id, routine.indexOf(exercise), routine.size()));
			exercise.exercise();
		}
		System.out.println(String.format("Client #%d has finished and is leaving the gym.", this.id));
	}
}
