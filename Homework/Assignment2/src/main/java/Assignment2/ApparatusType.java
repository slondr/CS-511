package Assignment2;

import java.util.Random;

public enum ApparatusType {
	LEGPRESSMACHINE,
	BARBELL,
	HACKSQUATMACHINE,
	LEGEXTENSIONMACHINE,
	LEGCURLMACHINE,
	LATPULLDOWNMACHINE,
	PECDECKMACHINE,
	CABLECROSSOVERMACHINE;
	
	public static ApparatusType generateRandomApparatus() {
		Random random = new Random();
		final int rnd = random.nextInt(8);
		return values()[rnd];
	}
}
