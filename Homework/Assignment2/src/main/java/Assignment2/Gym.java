package Assignment2;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * The Gym class represents the logical gym. Gyms have numerous clients and
 * apparatus. The apparatus make use of weight plates (defined in an enum) and
 * are controlled via semaphores.
 * By default, the total set of weight plates in the gym are:
 * 75 of size 10kg
 * 90 of size 5kg
 * 110 of size 3kg
 * This information is stored in noOfWeightPlates. Finally, each gym has a list
 * of clients and an executor.
 */
public class Gym implements Runnable {
	// default values are taken from assignment documentation
	private static final int GYM_SIZE = 30;
	private static final int GYM_REGISTERED_CLIENTS = 10000;
	private int current_id = 0;
	
	public static Map<WeightPlateSize, Integer> noOfWeightPlates;
	static {
		Map<WeightPlateSize, Integer> plates = new HashMap<WeightPlateSize, Integer>();
		plates.put(WeightPlateSize.SMALL_3KG, 110);
		plates.put(WeightPlateSize.MEDIUM_5KG, 90);
		plates.put(WeightPlateSize.LARGE_10KG, 75);
		noOfWeightPlates = java.util.Collections.unmodifiableMap(plates);
	}
	
	// these will be generated at runtime
	private Set<Integer> clients;
	private ExecutorService executor;
	
	// Semaphores to control access to apparatus
	public static Map<ApparatusType, Semaphore> apparatusAvailable = new HashMap<ApparatusType, Semaphore>();
	
	// Semaphores to control access to weights
	public static Map<WeightPlateSize, Semaphore> plateAvailable = new HashMap<WeightPlateSize, Semaphore>();
	
	// Semaphore to control access to semaphores
	public static Semaphore mutex = new Semaphore(1);
	
	public Gym() {
		// populate semaphore maps
		for (ApparatusType app : ApparatusType.values())
			apparatusAvailable.put(app, new Semaphore(5));
		// populate plate maps
		for (WeightPlateSize plate : WeightPlateSize.values())
			plateAvailable.put(plate, new Semaphore(noOfWeightPlates.get(plate)));
		
		// Generate unique client IDs
		clients = new java.util.HashSet<>();
		for (int i = 0; i < GYM_REGISTERED_CLIENTS; ++i)
			clients.add(i);
		
		executor = Executors.newFixedThreadPool(GYM_SIZE);
		
	}
	
	
	public void run() {
		for (int id : clients)
			executor.execute(Client.generateRandom(id));
	}
	
}
