package Assignment2;

import java.util.Map;
import java.util.Random;

/**
 * Each gym client has a `routine` consisting of a sequence of Exercises. An
 * exercise consists of an apparatus type, a corresponding weight, and a
 * duration in milliseconds.
 */
public class Exercise {
	private ApparatusType at;
	private Map<WeightPlateSize, Integer> weight;
	private int duration;
	
	public Exercise(ApparatusType at,
						 Map<WeightPlateSize, Integer> weight,
						 int duration) {
		
		this.at = at;
		this.weight = weight;
		this.duration = duration;
	}
	
	public void exercise() {
		// do the exercise
		try {
			// Acquiring multiple semaphores must be atomic to prevent livelock
			Gym.mutex.acquire();
			// Acquire the apparatus we want
			Gym.apparatusAvailable.get(at).acquire();
			// Acquire all the plates we need
			this.weight.forEach((k, v) -> {
				// not allowed to acquire(v), so we need to do this garbage
				for (int i = 0; i < v; ++i) {
					try {
						Gym.plateAvailable.get(k).acquire();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			});
			// we're done acquiring semaphores, so we can release the mutex
			Gym.mutex.release();
			
			// all the semaphores are acquired, so we can exercise
			Thread.sleep(duration); // sleeping is how I exercise in real life too!
			
			// we're done exercising, so now we release all of the semaphores
			Gym.apparatusAvailable.get(at).release();
			this.weight.forEach((k, v) -> {
				for (int i = 0; i < v; ++i)
					Gym.plateAvailable.get(k).release();
			});
		} catch (InterruptedException except) {
			except.printStackTrace();
		}
	}
	
	public static Exercise generateRandom() {
		Random random = new Random();
		// the apparatus to be used
		final ApparatusType apparatus = ApparatusType.generateRandomApparatus();
		
		// the weightMap which defines how many of each weight will be used
		Map<WeightPlateSize, Integer> weightMap;
		weightMap = new java.util.HashMap<WeightPlateSize, Integer>();
		for (WeightPlateSize weightPlateSize : WeightPlateSize.values())
			weightMap.put(weightPlateSize, random.nextInt(11));
		if (weightMap.values().stream().mapToInt(Integer::intValue).sum() == 0) {
			// if we made it here, it means that every weightPlateSize amount is 0
			// so we just randomly flip one to 1
			weightMap.replace(WeightPlateSize.generateRandomWeightPlateSize(), 1);
		}
		
		/* The duration for the new exercise.
		   Revisit this value if the simulation takes too long or finishes to fast.
		*/
		final int dur = random.nextInt(2000);
		return new Exercise(apparatus, weightMap, dur);
	}
	
}
