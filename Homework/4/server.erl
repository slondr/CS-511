-module(server).

-export([start_server/0]).

-include_lib("./defs.hrl").

-spec start_server() -> _.
-spec loop(_State) -> _.
-spec do_join(_ChatName, _ClientPID, _Ref, _State) -> _.
-spec do_leave(_ChatName, _ClientPID, _Ref, _State) -> _.
-spec do_new_nick(_State, _Ref, _ClientPID, _NewNick) -> _.
-spec do_client_quit(_State, _Ref, _ClientPID) -> _NewState.

start_server() ->
    catch(unregister(server)),
    register(server, self()),
    case whereis(testsuite) of
				undefined -> ok;
				TestSuitePID -> TestSuitePID!{server_up, self()}
    end,
    loop(
      #serv_st{
				 nicks = maps:new(), %% nickname map. client_pid => "nickname"
				 registrations = maps:new(), %% registration map. "chat_name" => [client_pids]
				 chatrooms = maps:new() %% chatroom map. "chat_name" => chat_pid
				}
     ).

loop(State) ->
    receive 
				%% initial connection
				{ClientPID, connect, ClientNick} ->
						NewState =
								#serv_st{
									 nicks = maps:put(ClientPID, ClientNick, State#serv_st.nicks),
									 registrations = State#serv_st.registrations,
									 chatrooms = State#serv_st.chatrooms
									},
						loop(NewState);
				%% client requests to join a chat
				{ClientPID, Ref, join, ChatName} ->
						NewState = do_join(ChatName, ClientPID, Ref, State),
						loop(NewState);
				%% client requests to join a chat
				{ClientPID, Ref, leave, ChatName} ->
						NewState = do_leave(ChatName, ClientPID, Ref, State),
						loop(NewState);
				%% client requests to register a new nickname
				{ClientPID, Ref, nick, NewNick} ->
						NewState = do_new_nick(State, Ref, ClientPID, NewNick),
						loop(NewState);
				%% client requests to quit
				{ClientPID, Ref, quit} ->
						NewState = do_client_quit(State, Ref, ClientPID),
						loop(NewState);
				{TEST_PID, get_state} ->
						TEST_PID!{get_state, State},
						loop(State)
    end.

%% executes join protocol from server perspective
do_join(ChatName, ClientPID, Ref, State) ->
		ChatRoomPID = case lists:member(ChatName, maps:keys(State#serv_st.chatrooms)) of
											true -> maps:get(ChatName, State#serv_st.chatrooms);
											false -> spawn(chatroom, start_chatroom, [ChatName])
									end,
		ClientNick = case maps:find(ClientPID, State#serv_st.nicks) of
										 {ok, V} -> V
								 end,
		ChatRoomPID!{self(), Ref, register, ClientPID, ClientNick},
		#serv_st{
			 nicks = State#serv_st.nicks,
			 registrations = case maps:find(ChatName, State#serv_st.registrations) of
													 {ok, Value} ->
															 %% ChatName was already registered
															 maps:put(ChatName, [ClientPID] ++ Value, State#serv_st.registrations);
													 error ->
															 %% ChatName was not registered
															 maps:put(ChatName, [ClientPID], State#serv_st.registrations)
											 end,
			 chatrooms = case maps:is_key(ChatName, State#serv_st.chatrooms) of
											 true -> State#serv_st.chatrooms;
											 false -> maps:put(ChatName, ChatRoomPID, State#serv_st.chatrooms)
									 end
			}.
													 
%% executes leave protocol from server perspective
do_leave(ChatName, ClientPID, Ref, State) ->
		ChatRoomPID = maps:get(ChatName, State#serv_st.chatrooms),
		NewState = #serv_st{
									nicks = State#serv_st.nicks,
									registrations = maps:put(ChatName, maps:get(ChatName, State#serv_st.registrations) -- [ClientPID], State#serv_st.registrations),
									chatrooms = State#serv_st.chatrooms
								 },
		ChatRoomPID!{self(), Ref, unregister, ClientPID},
		ClientPID!{self(), Ref, ack_leave},
    NewState.

%% executes new nickname protocol from server perspective
do_new_nick(State, Ref, ClientPID, NewNick) ->
    io:format("Client ~p has asked the server to be known as '~s'~n", [ClientPID, NewNick]),
		case lists:member(NewNick, maps:values(State#serv_st.nicks)) of
																								%maps:is_key(NewNick, State#serv_st.nicks) of
				true -> ClientPID!{self(), Ref, err_nick_used}, State;
				false -> 
						%% Update the chatrooms with the new nickname
						lists:foreach(fun(X) ->
																	maps:get(X, State#serv_st.chatrooms)!{self(), Ref, update_nick, ClientPID, NewNick}
													end, maps:keys(State#serv_st.chatrooms)),
						ClientPID!{self(), Ref, ok_nick},
						#serv_st{
							 nicks = maps:put(ClientPID, NewNick, State#serv_st.nicks),
							 registrations = State#serv_st.registrations, % re-use old regs
							 chatrooms = State#serv_st.chatrooms
							}
		end.

get_chatrooms_by_pid(State, Pid) ->
		%% Returns a list of chatroom PIDs that the given client PID is a member of
		lists:filter(fun(X) -> lists:member(Pid, X) end,
								 maps:keys(State#serv_st.registrations)).

%% executes client quit protocol from server perspective
do_client_quit(State, Ref, ClientPID) ->
		lists:foreach(fun(X) -> X!{self(), Ref, unregister, ClientPID} end,
									get_chatrooms_by_pid(State, ClientPID)),
		ClientPID!{self(), Ref, ack_quit},
		#serv_st{
			 nicks = maps:remove(ClientPID, State#serv_st.nicks),
			 registrations = maps:map(fun(_, V) -> case lists:member(ClientPID, V) of
																								 true -> V -- [ClientPID];
																								 false -> V
																						 end
																end,
																State#serv_st.registrations),
			 chatrooms = State#serv_st.chatrooms
			}.
