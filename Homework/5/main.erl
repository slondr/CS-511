-module(main).
-author("Eric S. Londres").
-compile(export_all).


setup_loop(0, _) ->
    out_of_sensors;
setup_loop(_, 0) ->
    done;
setup_loop(Sensors, Watchers)  ->
    case ((Sensors div 10) < 1) of
	true ->
	    spawn(watcher, watcher, [Sensors]),
	    setup_loop(0, Watchers);
	false ->
	    spawn(watcher, watcher, [10]),
	    setup_loop(Sensors - 10, Watchers - 1)
    end,
    todo.






start() ->
    {ok, [N]} = io:fread("enter number of sensors> ", "~d"),
    if N =< 1 ->
	    io:fwrite("setup: range must be at least 2~n", []);
       true ->
	    Num_watchers = 1 + (N div 10),
	    setup_loop(N, Num_watchers)
    end.
