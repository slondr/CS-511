public class Swapper implements Runnable {
    private int offset;
    private Interval interval;
    private String content;
    private char[] buffer;

    public Swapper(Interval interval, String content, char[] buffer, int offset) {
	// Offset specifies the starting index in the buffer where the content will be placed
        this.offset = offset;

	// Interval specifies the starting and ending index of the content in the original file to be swapped
        this.interval = interval;

	// The entire original file
	this.content = content;

	// The shared buffer space that is being written to
	this.buffer = buffer;
    }

    private String getChunk() {
	// Retrieve the target data from the content
	return this.content.substring(this.interval.getX(), this.interval.getY());
    }

    private void writeChunk(String data, int chunkSize) {
	for(int i = 0; i < chunkSize; ++i) {
	    this.buffer[i + offset] = data.charAt(i);
	}
    }
    
    @Override
    public void run() {
	this.writeChunk(this.getChunk(), this.interval.getY() - this.interval.getX());
    }
}
