-module(shipping).
-compile(export_all).
-include_lib("./shipping.hrl").

locate_ship(Ships, Ship_ID) ->
    case Ships of
				[H|T] ->
						case H of
								{ship, Ship_ID, _, _} -> H;
								_ -> locate_ship(T, Ship_ID)
						end;
				[] -> io:format("No ship found."),
							error
    end.
get_ship(Shipping_State, Ship_ID) ->
    locate_ship(Shipping_State#shipping_state.ships, Ship_ID).



locate_container(Containers, ID) ->
    case Containers of
				[H|T] ->
						case H of
								{container, ID, _} -> H;
								_ -> locate_container(T, ID)
						end;
				[] -> io:format("Container not found "),
							error
    end.
get_container(Shipping_State, Container_ID) ->
    locate_container(Shipping_State#shipping_state.containers, Container_ID).

locate_port(Ports, ID) ->
    case Ports of
				[H|T] ->
						case H of
								{port, ID, _, _, _} -> H;
								_ -> locate_port(T, ID)
						end;
				[] -> io:format("Port not found "),
							error
    end.	    
get_port(Shipping_State, Port_ID) ->
    locate_port(Shipping_State#shipping_state.ports, Port_ID).

collect_docks(Docks, ID, Collected_Docks) ->
    case Docks of
				[H|T] ->
						case H of
								{ID, Dock, _} ->
										collect_docks(T, ID, lists:flatten(Collected_Docks ++ [Dock]));
								_ -> collect_docks(T, ID, Collected_Docks)
						end;
				[] -> Collected_Docks
    end.
get_occupied_docks(Shipping_State, Port_ID) ->
    collect_docks(Shipping_State#shipping_state.ship_locations, Port_ID, []).

locate_ship_at_dock(Docks, ID) ->
    case Docks of
				[H|T] ->
						case H of
								{Port, Dock, ID} -> {Port, Dock};
								_  -> locate_ship_at_dock(T, ID)
						end;
				[] -> io:format("Dock cannot be found "),
							error
    end.
get_ship_location(Shipping_State, Ship_ID) ->
    locate_ship_at_dock(Shipping_State#shipping_state.ship_locations, Ship_ID).


sum_containers(Containers, IDs, Sum) ->
    case Containers of
				[H|T] ->
						case H of
								{container, ID, Weight} ->
										case lists:member(ID, IDs) of
												true -> sum_containers(T, IDs, Sum + Weight);
												false -> sum_containers(T, IDs, Sum)
										end;
								_ -> error
						end;
				[] -> Sum
    end.
get_container_weight(Shipping_State, Container_IDs) ->
    sum_containers(Shipping_State#shipping_state.containers, Container_IDs, 0).

get_ship_weight(Shipping_State, Ship_ID) ->
    case maps:find(Ship_ID, Shipping_State#shipping_state.ship_inventory) of
				{ok, R} -> get_container_weight(Shipping_State, R);
				error -> error
    end.

new_ship_inventory(Shipping_State, Capacity, Ship_Inventory, Containers) ->
    case Containers of
				[H|T] ->
						Len = length(Ship_Inventory),
						case Len >= Capacity of
								true -> io:format("error"), error;
								false -> new_ship_inventory(Shipping_State, Capacity, lists:flatten(Ship_Inventory ++ [H]), T)
						end;
				[] -> Ship_Inventory
    end.
load_ship(Shipping_State, Ship_ID, Container_IDs) ->
    Ships = Shipping_State#shipping_state.ships,
    Containers = Shipping_State#shipping_state.containers,
    Ports = Shipping_State#shipping_state.ports,
    Ship_Locations = Shipping_State#shipping_state.ship_locations,
    case get_ship(Shipping_State, Ship_ID) of
				{ship, Ship_ID, _, Capacity} ->
						case new_ship_inventory(Shipping_State,
																		Capacity,
																		maps:get(Ship_ID,
																						 Shipping_State#shipping_state.ship_inventory),
																		Container_IDs) of

								error -> error;
								New_Ship_Inventory ->
										Ship_Inventory = maps:put(Ship_ID, New_Ship_Inventory, Shipping_State#shipping_state.ship_inventory),
										case get_ship_location(Shipping_State, Ship_ID) of
												{PortID, _} ->
														Port_Inventory = maps:put(PortID,
																											lists:filter(fun(X) ->
																																					 not lists:member(X, Container_IDs)
																																	 end,
																																	 maps:get(PortID, Shipping_State#shipping_state.port_inventory)),
																											Shipping_State#shipping_state.port_inventory),
																								% Return the new shipping state
														#shipping_state{
															 ships = Ships,
															 containers = Containers,
															 ports = Ports,
															 ship_locations = Ship_Locations,
															 ship_inventory = Ship_Inventory,
															 port_inventory = Port_Inventory
															};
												_ -> error
										end
						end;
				_ -> error
    end.



new_port_inventory(Shipping_State, Capacity, Port_Inventory, Containers) ->
		case Containers of
				[H|T] ->
						case length(Port_Inventory) >= Capacity of
								true -> error;
								false -> new_port_inventory(Shipping_State, Capacity, lists:flatten(Port_Inventory ++ [H]), T)
						end;
				[] -> Port_Inventory
		end.

get_containers_by_ship(Shipping_State, Ship_ID) ->
		ShipMap = Shipping_State#shipping_state.ship_inventory,
		maps:get(Ship_ID, ShipMap).

unload_ship_all(Shipping_State, Ship_ID) ->
		unload_ship(Shipping_State, Ship_ID, get_containers_by_ship(Shipping_State, Ship_ID)).
						

unload_ship(Shipping_State, Ship_ID, Container_IDs) ->
		PInv = Shipping_State#shipping_state.port_inventory,
		case get_ship_location(Shipping_State, Ship_ID) of
				{Port, _} ->
						case get_port(Shipping_State, Port) of
								{port, Port, _, _, Capacity} -> 
										case new_port_inventory(Shipping_State, Capacity, maps:get(Port, PInv), Container_IDs) of
												error -> error;
												New_Port_Inventory ->
														Port_Inventory = maps:put(Port, New_Port_Inventory, PInv),
														Ship_Inventory = maps:put(Ship_ID,
																											lists:filter(fun(X) -> not lists:member(X, Container_IDs) end,
																																	 maps:get(Ship_ID,
																																						Shipping_State#shipping_state.ship_inventory)),
																											Shipping_State#shipping_state.ship_inventory),
														% return the new shipping_state
														#shipping_state{
															 ships = Shipping_State#shipping_state.ships,
															 containers = Shipping_State#shipping_state.containers,
															 ports = Shipping_State#shipping_state.ports,
															 ship_locations = Shipping_State#shipping_state.ship_locations,
															 ship_inventory = Ship_Inventory,
															 port_inventory = Port_Inventory
															}
										end;
								_ -> io:format("E "), error
						end;
				_ -> error
		end.

dock_is_empty(Shipping_State, {Port, Dock}) ->
		not lists:member(Dock, get_occupied_docks(Shipping_State, Port)).
									
						

set_sail(Shipping_State, Ship_ID, {Port_ID, Dock}) ->
		case dock_is_empty(Shipping_State, {Port_ID, Dock}) of
				false -> error;
				true -> #shipping_state{
									 ships = Shipping_State#shipping_state.ships,
									 containers = Shipping_State#shipping_state.containers,
									 ports = Shipping_State#shipping_state.ports,
									 ship_locations = lists:filter(fun(X) -> case X of
																															 {_, _, Ship_ID} -> false;
																															 _ -> true
																													 end
																								 end, lists:map(fun(X) -> case X of
																																							{Port_ID, _, _} -> {Port_ID, Dock, Ship_ID};
																																							A -> A
																																					end
																																end, Shipping_State#shipping_state.ship_locations)),
									 ship_inventory = Shipping_State#shipping_state.ship_inventory,
									 port_inventory = Shipping_State#shipping_state.port_inventory
									}
		end.
																								 
																								 

%% Determines whether all of the elements of Sub_List are also elements of Target_List
%% @returns true is all elements of Sub_List are members of Target_List; false otherwise
is_sublist(Target_List, Sub_List) ->
    lists:all(fun (Elem) -> lists:member(Elem, Target_List) end, Sub_List).


%% Prints out the current shipping state in a more friendly format
print_state(Shipping_State) ->
    io:format("--Ships--~n"),
    _ = print_ships(Shipping_State#shipping_state.ships, Shipping_State#shipping_state.ship_locations, Shipping_State#shipping_state.ship_inventory, Shipping_State#shipping_state.ports),
    io:format("--Ports--~n"),
    _ = print_ports(Shipping_State#shipping_state.ports, Shipping_State#shipping_state.port_inventory).


%% helper function for print_ships
get_port_helper([], _Port_ID) -> error;
get_port_helper([ Port = #port{id = Port_ID} | _ ], Port_ID) -> Port;
get_port_helper( [_ | Other_Ports ], Port_ID) -> get_port_helper(Other_Ports, Port_ID).


print_ships(Ships, Locations, Inventory, Ports) ->
    case Ships of
        [] ->
            ok;
        [Ship | Other_Ships] ->
            {Port_ID, Dock_ID, _} = lists:keyfind(Ship#ship.id, 3, Locations),
            Port = get_port_helper(Ports, Port_ID),
            {ok, Ship_Inventory} = maps:find(Ship#ship.id, Inventory),
            io:format("Name: ~s(#~w)    Location: Port ~s, Dock ~s    Inventory: ~w~n", [Ship#ship.name, Ship#ship.id, Port#port.name, Dock_ID, Ship_Inventory]),
            print_ships(Other_Ships, Locations, Inventory, Ports)
    end.

print_containers(Containers) ->
    io:format("~w~n", [Containers]).

print_ports(Ports, Inventory) ->
    case Ports of
        [] ->
            ok;
        [Port | Other_Ports] ->
            {ok, Port_Inventory} = maps:find(Port#port.id, Inventory),
            io:format("Name: ~s(#~w)    Docks: ~w    Inventory: ~w~n", [Port#port.name, Port#port.id, Port#port.docks, Port_Inventory]),
            print_ports(Other_Ports, Inventory)
    end.
%% This functions sets up an initial state for this shipping simulation. You can add, remove, or modidfy any of this content. This is provided to you to save some time.
%% @returns {ok, shipping_state} where shipping_state is a shipping_state record with all the initial content.
shipco() ->
    Ships = [#ship{id=1,name="Santa Maria",container_cap=20},
						 #ship{id=2,name="Nina",container_cap=20},
						 #ship{id=3,name="Pinta",container_cap=20},
						 #ship{id=4,name="SS Minnow",container_cap=20},
						 #ship{id=5,name="Sir Leaks-A-Lot",container_cap=20}
						],
    Containers = [
                  #container{id=1,weight=200},
                  #container{id=2,weight=215},
                  #container{id=3,weight=131},
                  #container{id=4,weight=62},
                  #container{id=5,weight=112},
                  #container{id=6,weight=217},
                  #container{id=7,weight=61},
                  #container{id=8,weight=99},
                  #container{id=9,weight=82},
                  #container{id=10,weight=185},
                  #container{id=11,weight=282},
                  #container{id=12,weight=312},
                  #container{id=13,weight=283},
                  #container{id=14,weight=331},
                  #container{id=15,weight=136},
                  #container{id=16,weight=200},
                  #container{id=17,weight=215},
                  #container{id=18,weight=131},
                  #container{id=19,weight=62},
                  #container{id=20,weight=112},
                  #container{id=21,weight=217},
                  #container{id=22,weight=61},
                  #container{id=23,weight=99},
                  #container{id=24,weight=82},
                  #container{id=25,weight=185},
                  #container{id=26,weight=282},
                  #container{id=27,weight=312},
                  #container{id=28,weight=283},
                  #container{id=29,weight=331},
                  #container{id=30,weight=136}
                 ],
    Ports = [
             #port{
                id=1,
                name="New York",
                docks=['A','B','C','D'],
                container_cap=200
               },
             #port{
                id=2,
                name="San Francisco",
                docks=['A','B','C','D'],
                container_cap=200
               },
             #port{
                id=3,
                name="Miami",
                docks=['A','B','C','D'],
                container_cap=200
               }
            ],
    %% {port, dock, ship}
    Locations = [
                 {1,'B',1},
                 {1, 'A', 3},
                 {3, 'C', 2},
                 {2, 'D', 4},
                 {2, 'B', 5}
                ],
    Ship_Inventory = #{
											 1=>[14,15,9,2,6],
											 2=>[1,3,4,13],
											 3=>[],
											 4=>[2,8,11,7],
											 5=>[5,10,12]},
    Port_Inventory = #{
											 1=>[16,17,18,19,20],
											 2=>[21,22,23,24,25],
											 3=>[26,27,28,29,30]
											},
    #shipping_state{ships = Ships, containers = Containers, ports = Ports, ship_locations = Locations, ship_inventory = Ship_Inventory, port_inventory = Port_Inventory}.
